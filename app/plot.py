import pylab
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
from matplotlib import style
from app import config

matplotlib.use("TkAgg")
LARGE_FONT = ("Verdana", 12)
style.use("seaborn-dark")


def find_ylims(xrange, x, y):
    l, r = xrange
    y_cut = [y for x, y in zip(x, y) if l < x < r]
    if y_cut:
        return 0.999, 1.1 * max(y_cut)
    return None


def find_xlims(x, y):
    x_cut = [x for x, y in zip(x, y) if y > 0]
    if x_cut:
        return 0.95 * min(x_cut), 1.05 * max(x_cut)
    return None


def config_plot():
    plt.grid(linestyle='--')
    plt.xlabel("Time difference, ps")
    plt.ylabel("Counts")
    plt.title("TAC Control Software")
    plt.tight_layout()


class TdcPlot():
    def __init__(self, tdc):
        self.tdc = tdc
        w = config.TDC_WIDTH
        self.prev_y = [0] * w

        self.ax = pylab.gca()
        # self.hist = self.ax.bar(range(0, w * config.W, config.W), [0] * w,
        #                         width=config.W,
        #                         alpha=0.7)
        # self.hist = self.ax.scatter([], [])
        self.hist = self.ax.plot([], [],
                                 lw=2,
                                 color='orange',
                                 marker='o',
                                 markersize=5,
                                 markerfacecolor='green',
                                 markeredgecolor='green')[0]
        self.fig = plt.gcf()
        self.xlims = 0, 140000
        self.ax.set_xlim(*self.xlims)
        self.xdata, self.ydata = [], []
        self.animation = None
        self.auto_y = True
        self.auto_x = False
        self.logscale = False
        self.reset = False

        config_plot()

    def run(self, i):
        if not self.reset:
            x, y = self.tdc.get_data()
            x = np.array(x[1:])
            y = np.array(y[1:])
            # mask = y > 0
            # x = x[mask]
            # y = y[mask]
        else:
            x, y = np.array([0] * config.TDC_WIDTH), np.array([0] *
                                                              config.TDC_WIDTH)
            self.prev_y = [0] * config.TDC_WIDTH
            self.reset = False
        # self.hist[0].set_ydata(y)
        # for j, (rect, yi) in enumerate(zip(self.hist, y)):
        #     # if not yi == self.prev_y[j]:
        #     if yi > 0:
        #         rect.set_height(yi)
        # self.prev_y = y
        self.hist.set_data(x, y)
        # self.hist.set_offsets(np.c_[x, y])

        ylimits = find_ylims(self.ax.get_xlim(), x, y) or config.YLIMS
        xlimits = find_xlims(x, y) or config.XLIMS

        self.ax.autoscale_view()
        if self.auto_x:
            self.ax.set_xlim(*xlimits)
            self.auto_x = False
        if self.auto_y:
            self.ax.set_ylim(*ylimits)
        if self.logscale:
            self.ax.set_yscale('log')
        else:
            self.ax.set_yscale('linear')
        return self.hist

    def start_animation(self):
        self.animation = animation.FuncAnimation(self.fig,
                                                 self.run,
                                                 blit=False,
                                                 interval=1000)

    def reset_plot(self):
        self.reset = True
