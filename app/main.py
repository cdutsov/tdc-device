from app import plot, gui, read_tdc, measurement


def run():
    tdc = read_tdc.Tdc()
    io = measurement.IO(tdc)
    print('TDC initialized')
    plt = plot.TdcPlot(tdc)
    print('Plotting...')
    window = gui.TdcGui(plt, tdc, io)
    plt.start_animation()
    window.start_mainloop()
