class IO():
    def __init__(self, tdc):
        self.tdc = tdc

    def save(self, filename):
        xdata = self.tdc.x
        ydata = self.tdc.y
        with open(filename, 'w') as f:
            for x, y in zip(xdata, ydata):
                s = '{:8.3f}\t'.format(x / 1000)
                s += '{:8d}\n'.format(y)
                f.write(s)
