import sys
import os
import tkinter as tk
from tkinter import messagebox
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg,\
    NavigationToolbar2Tk
from app import config


class TdcGui():
    def __init__(self, plt, tdc, io):
        self.root = tk.Tk()

        self.root.title("TDC control center")
        self.root.minsize(1200, 1000)

        # self.root.pack(side="top", fill="both", expand=True)
        # self.root.grid_rowconfigure(0, weight=1)
        # self.root.grid_columnconfigure(0, weight=1)
        self.plot = plt
        self.tdc = tdc
        self.io = io

        canvas = FigureCanvasTkAgg(plt.fig, self.root)
        # canvas.show()
        canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

        toolbar = NavigationToolbar2Tk(canvas, self.root)
        toolbar.update()
        canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        self.running = False
        self.start_stop_btn = tk.Button(self.root, text='Start',
                                        bg='green',
                                        command=self.start_stop_tdc)

        self.connected = False
        self.conn_btn = tk.Button(self.root, text='Connect',
                                  bg='green',
                                  command=self.conn_disc_tdc)
        self.conn_btn.pack(side=tk.LEFT)

        self.paused = False
        self.b = tk.Button(self.root, text='Pause',
                           command=self.pause_animation)
        self.b.pack(side=tk.LEFT)

        clear_btn = tk.Button(self.root, text='Clear',
                              command=self.clear_hist)
        clear_btn.pack(side=tk.LEFT)

        fullx_btn = tk.Button(self.root, text='Full X range',
                              command=self.reset_x)
        fullx_btn.pack(side=tk.LEFT)

        self.autox_btn = tk.Button(self.root, text='Auto X range',
                                   command=self.toggle_autox)
        self.autox_btn.pack(side=tk.LEFT)

        self.autoy_btn = tk.Button(self.root, text='Auto Y range',
                                   bg='green', command=self.toggle_autoy)
        self.autoy_btn.pack(side=tk.LEFT)

        self.toggle_logscale_btn = tk.Button(self.root, text='Log Y',
                                             command=self.toggle_log)
        self.toggle_logscale_btn.pack(side=tk.LEFT)

        self.save_button = tk.Button(self.root, text='Save Spectrum',
                                     command=self.save_spectrum)
        self.save_button.pack(side=tk.RIGHT)

        self.root.protocol("WM_DELETE_WINDOW", self.on_closing)

    def start_mainloop(self):
        self.root.mainloop()

    def conn_disc_tdc(self):
        if not self.connected:
            port = None
            for p in config.PORTS:
                if os.path.exists(p):
                    port = p
            self.connected = self.tdc.connect_tdc(port)
            if self.connected:
                self.conn_btn.configure(text='Disconnect', bg='red')
                self.start_stop_btn.pack(side=tk.LEFT)
                self.tdc.update_tdc_result()
            self.start_stop_btn.configure(state='normal')
        else:
            self.tdc.stop_tdc()
            self.tdc.disconnect_tdc()
            self.conn_btn.configure(text='Connect', bg='green')
            self.connected = False
            self.start_stop_btn.configure(text='Start',
                                          state='disabled', bg='red')

    def start_stop_tdc(self):
        self.running ^= True
        if self.running:
            self.start_stop_btn.configure(text='Stop', bg='red')
            self.tdc.start_tdc()
        else:
            self.start_stop_btn.configure(text='Start', bg='green')
            self.tdc.stop_tdc()

    def save_spectrum(self):
        filename = tk.filedialog.\
            asksaveasfilename(
                initialdir='/home/chavdar',
                title='Save file',
                filetypes=(('DAT files', '*.dat'),
                           ('TXT files', '*.txt'),
                           ('All files', '*.*')))
        self.io.save(filename)

    def toggle_log(self):
        self.plot.logscale ^= True
        log = self.plot.logscale
        if log:
            self.toggle_logscale_btn.configure(text='Linear Y')
        else:
            self.toggle_logscale_btn.configure(text='Log Y')

    def reset_x(self):
        self.plot.ax.set_xlim(0, 140000)

    def toggle_autox(self):
        self.plot.auto_x ^= True

    def toggle_autoy(self):
        self.plot.auto_y ^= True
        auto_y = self.plot.auto_y
        if auto_y:
            self.autoy_btn.configure(bg='green')
        else:
            self.autoy_btn.configure(bg='red')

    def on_closing(self):
        if messagebox.askokcancel("Quit", "Do you realy want to quit?"):
            self.plot.animation.event_source.stop()
            self.tdc.stop_tdc()
            self.root.destroy()
            sys.exit(0)

    def pause_animation(self):
        self.paused ^= True
        if self.paused:
            self.plot.animation.event_source.stop()
            self.b.configure(text='Resume')
        else:
            self.plot.animation.event_source.start()
            self.b.configure(text='Pause')

    def clear_hist(self):
        self.plot.reset_plot()
        self.tdc.clear_tdc()
