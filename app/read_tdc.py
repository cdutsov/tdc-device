from collections import OrderedDict
import numpy as np
import threading
import time
import serial
from app import config


class Tdc():
    def __init__(self):
        self.histogram = [], []
        self.x = [0]*config.TDC_WIDTH
        self.y = [0]*config.TDC_WIDTH
        self.ser = None
        self.thread = None

    # def __del__(self):
    #     self.thread.join()

    def get_data(self):
        # self.update_data()
        return self.x, self.y

    def clear_data(self):
        self.x = [0]*config.TDC_WIDTH
        self.y = [0]*config.TDC_WIDTH

    def connect_tdc(self, port):
        self.ser = serial.Serial(port, config.BAUD, timeout=5)
        print("TAC status: ", self.ser.isOpen())
        return self.ser.isOpen()

    def disconnect_tdc(self):
        self.ser.close()
        print("TAC status: ", self.ser.isOpen())
        return True

    def clear_tdc(self):
        self.clear_data()
        if self.ser.isOpen():
            self.ser.write(bytes('c'.encode('utf-8')))

    def start_tdc(self):
        print("sending start signal to device...", end='')
        self.ser.write(bytes('t'.encode('utf-8')))
        print('done')
        time.sleep(0.01)
        self.thread = MyThread(target=self.start, args=(self.ser, ))
        self.thread.start()

    def stop_tdc(self):
        if self.thread:
            self.thread.stop()
            print('thread stopped')
        if self.ser and self.ser.isOpen():
            self.ser.write(bytes('p'.encode('utf-8')))
            print('tdc stopped')

    def get_tdc(self):
        self.ser.write(bytes('a'.encode('utf-8')))

    def update_tdc_result(self):
        self.get_tdc()
        hist = OrderedDict()
        result = self.ser.read(config.TDC_WIDTH*4)
        for i, res in enumerate(zip(*[iter(result)]*4)):
            val = int.from_bytes(list(res), byteorder='little')
            hist.update({i: val})

        print("-----------")
        self.x = [int(x * config.W) for x in list(hist.keys())]
        self.y = [int(y) for y in hist.values()]
        time.sleep(1)

    def start(self, thr):
        print('tdc thread started', thr)
        while not self.thread.stopped():
            print('Waiting for TDC data')
            self.update_tdc_result()
            time.sleep(2)

    def stop(self):
        if self.thread.isAlive():
            self.thread.stop()


class MyThread(threading.Thread):
    """Thread class with a stop() method. The thread itself has to check
    regularly for the stopped() condition."""

    def __init__(self, *args, **kwargs):
        super(MyThread, self).__init__(*args, **kwargs)
        self._stop = threading.Event()

    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()
