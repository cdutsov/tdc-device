This is a code to control Time-to-Digital Converters that I built. One uses an Artix-7 FPGA and the other is a Texas Instruments TDC7200 based device. Both output a list of time intervals in picoseconds to a serial port. This program visualizes the histograms.

The interface of the program:
![interface](./interface.png)
